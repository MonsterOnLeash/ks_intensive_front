import React from "react";
import Cookies from "js-cookie";

const TOKEN_ACQUIRE_URL = "http://mawliot.pythonanywhere.com/api-token-auth/";

export default class LoginModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      login: "",
      password: "",
      success: true,
    };
  }

  handleChangeLogin(event) {
    this.setState({ login: event.target.value });
  }

  handleChangePass(event) {
    this.setState({ password: event.target.value });
  }

  LoginAttempt = async () => {
    const { EntryEvent, getTokenName } = this.props;
    try {
      const headers = new Headers();
      headers.append("content-type", "application/json");

      const body = JSON.stringify({
        username: this.state.login,
        password: this.state.password,
      });

      const init = {
        method: "POST",
        headers,
        body,
      };

      const response = await fetch(TOKEN_ACQUIRE_URL, init);
      let token = await response.json();
      if ("token" in token) {
        getTokenName(token.token, this.state.login);
        EntryEvent(true);
        Cookies.set("isLoggedIn", true);
      } else {
        this.setState({ success: false });
      }
    } catch (err) {
      alert(err);
    }
  };

  render() {
    return (
      <div>
        <h1 style={{ color: "white" }}>Форма Логина</h1>
        <div>
          <label style={{ color: "white" }}>
            Name:
            <input
              type="text"
              value={this.state.login}
              onChange={(event) => this.handleChangeLogin(event)}
              required
            ></input>
          </label>
        </div>
        <div>
          <label style={{ color: "white" }}>
            Password:
            <input
              type="password"
              value={this.state.password}
              onChange={(event) => this.handleChangePass(event)}
              required
            ></input>
          </label>
        </div>
        <div>
          <button onClick={this.LoginAttempt}>Log in</button>
        </div>
        {this.state.success ? (
          <div />
        ) : (
          <h3 style={{ color: "white" }}>Неправильный логин или пароль</h3>
        )}
      </div>
    );
  }
}
