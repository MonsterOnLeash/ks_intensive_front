import React from "react";
import { DataGrid } from "@mui/x-data-grid";
import Dropdown from "react-dropdown";
import "react-dropdown/style.css";

const TODOS_URL = "http://mawliot.pythonanywhere.com/api/taskstodo/todos/";
const AUTHORS_URL = "http://mawliot.pythonanywhere.com/api/taskstodo/authors/";
const TAGS_URL = "http://mawliot.pythonanywhere.com/api/taskstodo/tags/";

export default class Todos extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      token: this.props.token,
      data: [],
      authors: [],
      tags: [],
      chosenRows: [],
      title: "",
      text: "",
      author: "",
      tag: "",
      correctInput: true,
      authorOptions: [],
      tagOptions: [],
    };
  }

  findArrayElementById(array, title) {
    return array.find((element) => {
      return element.id === title;
    });
  }

  findArrayElementByName(array, title) {
    return array.find((element) => {
      return element.name === title;
    });
  }

  AuthorsLoader = async () => {
    try {
      const aresponse = await fetch(AUTHORS_URL, {
        headers: {
          Authorization: this.state.token,
          "content-type": "application/json",
        },
      });

      let authors = await aresponse.json();
      let authorOptions = [];
      for (let i = 0; i < authors.length; i++) {
        authorOptions.push(authors[i].name);
      }
      this.setState({ authorOptions: authorOptions });
      return authors;
    } catch (err) {
      alert(err);
    }
  };

  TagsLoader = async () => {
    try {
      const tresponse = await fetch(TAGS_URL, {
        headers: {
          Authorization: this.state.token,
          "content-type": "application/json",
        },
      });

      let tags = await tresponse.json();
      let tagOptions = [];
      for (let i = 0; i < tags.length; i++) {
        tagOptions.push(tags[i].name);
      }
      this.setState({ tagOptions: tagOptions });
      return tags;
    } catch (err) {
      alert(err);
    }
  };

  DataLoader = async () => {
    try {
      const response = await fetch(TODOS_URL, {
        headers: {
          Authorization: this.state.token,
          "content-type": "application/json",
        },
      });

      let data = await response.json();
      this.setState({ data: data });
      return data;
    } catch (err) {
      alert(err);
    }
  };

  DeleteRows = async () => {
    for (let i = 0; i < this.state.chosenRows.length; i++) {
      try {
        const response = await fetch(
          TODOS_URL + this.state.chosenRows[i] + "/",
          {
            method: "DELETE",
            headers: {
              Authorization: this.state.token,
              "content-type": "application/json",
            },
          }
        );
        this.updateAllData();
      } catch (err) {
        alert(err);
      }
    }
  };

  updateAllData = async () => {
    try {
      let data = await this.DataLoader();
      let authors = await this.AuthorsLoader();
      let tags = await this.TagsLoader();
      for (let i = 0; i < data.length; i++) {
        data[i].author = this.findArrayElementById(
          authors,
          data[i].author
        ).name;
        data[i].tag = this.findArrayElementById(tags, data[i].tag).name;
      }

      this.setState({ data: data, authors: authors, tags: tags });
    } catch (err) {
      alert(err);
    }
  };

  componentDidMount = async () => {
    this.updateAllData();
  };

  handleChangeTitle(event) {
    this.setState({ title: event.target.value });
  }

  handleChangeText(event) {
    this.setState({ text: event.target.value });
  }

  handleChangeAuthor(event) {
    this.setState({ author: event.target.value });
  }

  handleChangeTag(event) {
    this.setState({ tag: event.target.value });
  }

  AddRow = async () => {
    try {
      const response = await fetch(TODOS_URL, {
        method: "POST",
        headers: {
          Authorization: this.state.token,
          "content-type": "application/json",
        },
        body: JSON.stringify({
          title: this.state.title,
          text: this.state.text,
          author: this.findArrayElementByName(
            this.state.authors,
            this.state.author
          ).id,
          tag: this.findArrayElementByName(this.state.tags, this.state.tag).id,
        }),
      });

      let ans = await response.json();
      if ("date_of_creation" in ans) {
        this.setState({ correctInput: true });
        this.updateAllData();
      } else {
        this.setState({ correctInput: false });
      }
    } catch (err) {
      this.setState({ correctInput: false });
    }
  };

  ChangeRow = async () => {
    if (this.state.chosenRows.length == 1) {
      try {
        const response = await fetch(
          TODOS_URL + this.state.chosenRows[0] + "/",
          {
            method: "PUT",
            headers: {
              Authorization: this.state.token,
              "content-type": "application/json",
            },
            body: JSON.stringify({
              title: this.state.title,
              text: this.state.text,
              author: this.findArrayElementByName(
                this.state.authors,
                this.state.author
              ).id,
              tag: this.findArrayElementByName(this.state.tags, this.state.tag)
                .id,
            }),
          }
        );

        let ans = await response.json();
        if ("date_of_creation" in ans) {
          this.setState({ correctInput: true });
          this.updateAllData();
        } else {
          this.setState({ correctInput: false });
        }
      } catch (err) {
        alert(err);
        this.setState({ correctInput: false });
      }
    } else this.setState({ correctInput: false });
  };

  columns = [
    {
      field: "title",
      headerName: "title",
    },
    {
      field: "text",
      headerName: "text",
      width: 500,
    },
    {
      field: "date_of_creation",
      headerName: "date of creation",
      type: "dateTime",
      width: 150,
    },
    {
      field: "author",
      headerName: "author",
      width: 150,
    },
    {
      field: "tag",
      headerName: "tag",
      width: 150,
    },
  ];

  _onSelect(option) {
    this.setState({ author: option });
  }

  render() {
    const adefaultValue = this.state.author;
    const tdefaultValue = this.state.tag;
    return (
      <div style={{ height: 400, width: "100%" }}>
        <DataGrid
          getRowId={(row) => row.id}
          rows={this.state.data}
          columns={this.columns}
          pageSize={5}
          rowsPerPageOptions={[5, 10]}
          checkboxSelection
          onSelectionModelChange={(itm) => this.setState({ chosenRows: itm })}
        />
        <div>
          <button onClick={this.DeleteRows}>Delete Chosen Rows</button>
        </div>
        <h2>Add / Change Row</h2>
        <div>
          <label style={{ color: "black" }}>
            Title:
            <input
              type="text"
              value={this.state.title}
              onChange={(event) => this.handleChangeTitle(event)}
              required
            />
          </label>
          <label style={{ color: "black" }}>
            Text:
            <input
              type="text"
              value={this.state.text}
              onChange={(event) => this.handleChangeText(event)}
              required
            />
          </label>
          <label style={{ color: "black" }}>
            <div> Author: </div>
            <Dropdown
              options={this.state.authorOptions}
              onChange={(option) => this.setState({author: option.value})}
              value={adefaultValue}
              placeholder="Select an author"
            />
          </label>
          <label style={{ color: "black" }}>
            Tag:
            <Dropdown
              options={this.state.tagOptions}
              onChange={(option) => this.setState({tag: option.value})}
              value={tdefaultValue}
              placeholder="Select an author"
            />
          </label>
        </div>
        <div>
          <button onClick={this.ChangeRow}>Change Chosen Row</button>
          <button onClick={this.AddRow}>Add Row</button>
          {this.state.correctInput ? (
            <div />
          ) : (
            <h3 style={{ color: "Red" }}>Некорректные данные</h3>
          )}
        </div>
      </div>
    );
  }
}
