import React from "react";
import { DataGrid } from "@mui/x-data-grid";

const AUTHORS_URL = "http://mawliot.pythonanywhere.com/api/taskstodo/authors/";

export default class Todos extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      token: this.props.token,
      authors: [],
      chosenRows: [],
      name: "",
      title: "",
      vacation: false,
      correctInput: true,
    };
  }

  AuthorsLoader = async () => {
    try {
      const tresponse = await fetch(AUTHORS_URL, {
        headers: {
          Authorization: this.state.token,
          "content-type": "application/json",
        },
      });

      let authors = await tresponse.json();

      return authors;
    } catch (err) {
      alert(err);
    }
  };

  DeleteRows = async () => {
    for (let i = 0; i < this.state.chosenRows.length; i++) {
      try {
        const response = await fetch(
          AUTHORS_URL + this.state.chosenRows[i] + "/",
          {
            method: "DELETE",
            headers: {
              Authorization: this.state.token,
              "content-type": "application/json",
            },
          }
        );
        this.updateAllData();
      } catch (err) {
        alert(err);
      }
    }
  };

  updateAllData = async () => {
    try {
      let authors = await this.AuthorsLoader();

      this.setState({ authors: authors });
    } catch (err) {
      alert(err);
    }
  };

  componentDidMount = async () => {
    this.updateAllData();
  };

  handleChangeName(event) {
    this.setState({ name: event.target.value });
  }

  handleChangeTitle(event) {
    this.setState({ title: event.target.value });
  }

  handleChangeVacation(event) {
    this.setState({ vacation: event.target.value });
  }

  AddRow = async () => {
    try {
      const response = await fetch(AUTHORS_URL, {
        method: "POST",
        headers: {
          Authorization: this.state.token,
          "content-type": "application/json",
        },
        body: JSON.stringify({
          name: this.state.name,
          title: this.state.title,
          on_vacation: this.state.vacation,
        }),
      });
      let ans = await response.json();
      if ("id" in ans) {
        this.setState({ correctInput: true });
        this.updateAllData();
      } else {
        this.setState({ correctInput: false });
      }
    } catch (err) {
      this.setState({ correctInput: false });
    }
  };

  ChangeRow = async () => {
    if (this.state.chosenRows.length == 1) {
      try {
        const response = await fetch(
          AUTHORS_URL + this.state.chosenRows[0] + "/",
          {
            method: "PUT",
            headers: {
              Authorization: this.state.token,
              "content-type": "application/json",
            },
            body: JSON.stringify({
              name: this.state.name,
              title: this.state.title,
              on_vacation: this.state.vacation,
            }),
          }
        );

        let ans = await response.json();
        if ("id" in ans) {
          this.setState({ correctInput: true });
          this.updateAllData();
        } else {
          this.setState({ correctInput: false });
        }
      } catch (err) {
        alert(err);
        this.setState({ correctInput: false });
      }
    } else this.setState({ correctInput: false });
  };

  columns = [
    {
      field: "name",
      headerName: "name",
    },
    {
      field: "title",
      headerName: "title",
      width: 500,
    },
    {
      field: "on_vacation",
      headerName: "on vacation",
      type: "boolean",
      width: 150,
    },
  ];

  render() {
    return (
      <div style={{ height: 400, width: "100%" }}>
        <DataGrid
          getRowId={(row) => row.id}
          rows={this.state.authors}
          columns={this.columns}
          pageSize={5}
          rowsPerPageOptions={[5, 10]}
          checkboxSelection
          onSelectionModelChange={(itm) => this.setState({ chosenRows: itm })}
        />
        <div>
          <button onClick={this.DeleteRows}>Delete Chosen Rows</button>
        </div>
        <h2>Add / Change Row</h2>
        <div>
          <label style={{ color: "black" }}>
            Name:
            <input
              type="text"
              value={this.state.name}
              onChange={(event) => this.handleChangeName(event)}
              required
            />
          </label>
          <label style={{ color: "black" }}>
            Position:
            <input
              type="text"
              value={this.state.title}
              onChange={(event) => this.handleChangeTitle(event)}
            />
          </label>
          <label style={{ color: "black" }}>
            On Vacation:
            <input
              type="boolean"
              value={this.state.vacation}
              onChange={(event) => this.handleChangeVacation(event)}
            />
          </label>
        </div>
        <div>
          <button onClick={this.ChangeRow}>Change Chosen Row</button>
          <button onClick={this.AddRow}>Add Row</button>
          {this.state.correctInput ? (
            <div />
          ) : (
            <h3 style={{ color: "Red" }}>Некорректные данные</h3>
          )}
        </div>
      </div>
    );
  }
}
