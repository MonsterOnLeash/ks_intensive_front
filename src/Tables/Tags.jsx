import React from "react";
import { DataGrid } from "@mui/x-data-grid";

const TAGS_URL = "http://mawliot.pythonanywhere.com/api/taskstodo/tags/";

export default class Todos extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      token: this.props.token,
      tags: [],
      chosenRows: [],
      name: "",
      description: "",
      color: "",
      correctInput: true,
    };
  }

  TagsLoader = async () => {
    try {
      const tresponse = await fetch(TAGS_URL, {
        headers: {
          Authorization: this.state.token,
          "content-type": "application/json",
        },
      });

      let tags = await tresponse.json();

      return tags;
    } catch (err) {
      alert(err);
    }
  };

  DeleteRows = async () => {
    for (let i = 0; i < this.state.chosenRows.length; i++) {
      try {
        const response = await fetch(
          TAGS_URL + this.state.chosenRows[i] + "/",
          {
            method: "DELETE",
            headers: {
              Authorization: this.state.token,
              "content-type": "application/json",
            },
          }
        );
        this.updateAllData();
      } catch (err) {
        alert(err);
      }
    }
  };

  updateAllData = async () => {
    try {
      let tags = await this.TagsLoader();

      this.setState({ tags: tags });
    } catch (err) {
      alert(err);
    }
  };

  componentDidMount = async () => {
    this.updateAllData();
  };

  handleChangeName(event) {
    this.setState({ name: event.target.value });
  }

  handleChangeDescription(event) {
    this.setState({ description: event.target.value });
  }

  handleChangeColor(event) {
    this.setState({ color: event.target.value });
  }

  AddRow = async () => {
    try {
      const response = await fetch(TAGS_URL, {
        method: "POST",
        headers: {
          Authorization: this.state.token,
          "content-type": "application/json",
        },
        body: JSON.stringify({
          name: this.state.name,
          description: this.state.description,
          color: this.state.color,
        }),
      });
      let ans = await response.json();
      if ("id" in ans) {
        this.setState({ correctInput: true });
        this.updateAllData();
      } else {
        this.setState({ correctInput: false });
      }
    } catch (err) {
      this.setState({ correctInput: false });
    }
  };

  ChangeRow = async () => {
    if (this.state.chosenRows.length == 1) {
      try {
        const response = await fetch(
          TAGS_URL + this.state.chosenRows[0] + "/",
          {
            method: "PUT",
            headers: {
              Authorization: this.state.token,
              "content-type": "application/json",
            },
            body: JSON.stringify({
              name: this.state.name,
              description: this.state.description,
              color: this.state.color,
            }),
          }
        );

        let ans = await response.json();
        if ("id" in ans) {
          this.setState({ correctInput: true });
          this.updateAllData();
        } else {
          this.setState({ correctInput: false });
        }
      } catch (err) {
        alert(err);
        this.setState({ correctInput: false });
      }
    } else this.setState({ correctInput: false });
  };

  columns = [
    {
      field: "name",
      headerName: "name",
    },
    {
      field: "description",
      headerName: "description",
      width: 500,
    },
    {
      field: "color",
      headerName: "color",
      width: 150,
    },
  ];

  render() {
    return (
      <div style={{ height: 400, width: "100%" }}>
        <DataGrid
          getRowId={(row) => row.id}
          rows={this.state.tags}
          columns={this.columns}
          pageSize={5}
          rowsPerPageOptions={[5, 10]}
          checkboxSelection
          onSelectionModelChange={(itm) => this.setState({ chosenRows: itm })}
        />
        <div>
          <button onClick={this.DeleteRows}>Delete Chosen Rows</button>
        </div>
        <h2>Add / Change Row</h2>
        <div>
          <label style={{ color: "black" }}>
            Name:
            <input
              type="text"
              value={this.state.name}
              onChange={(event) => this.handleChangeName(event)}
              required
            />
          </label>
          <label style={{ color: "black" }}>
            Description:
            <input
              type="text"
              value={this.state.description}
              onChange={(event) => this.handleChangeDescription(event)}
            />
          </label>
          <label style={{ color: "black" }}>
            Color:
            <input
              type="text"
              value={this.state.color}
              onChange={(event) => this.handleChangeColor(event)}
            />
          </label>
        </div>
        <div>
          <button onClick={this.ChangeRow}>Change Chosen Row</button>
          <button onClick={this.AddRow}>Add Row</button>
          {this.state.correctInput ? (
            <div />
          ) : (
            <h3 style={{ color: "Red" }}>Некорректные данные</h3>
          )}
        </div>
      </div>
    );
  }
}
