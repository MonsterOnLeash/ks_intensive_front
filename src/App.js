import logo from "./logo.svg";
import "./App.css";
import LoginModal from "./loginmodal/LoginModal";
import React from "react";
import DashBoard from "./MainWindow/Dashboard";
import Cookies from "js-cookie";
import Navigator from "./MainWindow/Navigator";

class App extends React.Component {
  state = {
    isLoggedIn: Cookies.get("isLoggedIn") == "true" || false,
    username: Cookies.get("user") || "",
    token: Cookies.get("token") || "",
		activeTable: 1,
  };

  getUserToken = (token, user) => {
    this.setState({ username: user, token: "Token " + token });
    Cookies.set("token", "Token " + token);
    Cookies.set("user", user);
  };

  handleLoginClick = (param) => {
    this.setState({ isLoggedIn: param });
  };

	ExitAttempt = () => {
    this.handleLoginClick(false);
    Cookies.remove("token");
    Cookies.remove("user");
    Cookies.set("isLoggedIn", false);
  };

	SetTable = (params) => {
		this.setState({activeTable: params});
	}

  render() {
    return (
      <div className="App">
        {this.state.isLoggedIn ? (
          <div className="mainpage">
            <div>
              {this.state.username}
              <button onClick={this.ExitAttempt}> Exit </button>
            </div>
            <Navigator switchFunction = {this.SetTable}/>
            <DashBoard
              token={this.state.token}
							table={this.state.activeTable}
            ></DashBoard>
          </div>
        ) : (
          <div className="loginpage">
            <LoginModal
              EntryEvent={this.handleLoginClick}
              getTokenName={this.getUserToken}
            ></LoginModal>
          </div>
        )}
      </div>
    );
  }
}
export default App;
