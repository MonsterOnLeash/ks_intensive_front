import React from "react";
import PlaylistAddCheckIcon from "@mui/icons-material/PlaylistAddCheck";
import TagIcon from "@mui/icons-material/Tag";
import PersonIcon from "@mui/icons-material/Person";

export default class Navigator extends React.Component {
  SwitchTables = (value) => {
    const { switchFunction } = this.props;
    switchFunction(value);
  }

   ButtonArray = [
    {
      text: "TODOS",
      icon: <PlaylistAddCheckIcon />,
      value: 1,
    },
    {
      text: "Tags",
      icon: <TagIcon />,
      value: 2,
    },
    {
      text: "Authors",
      icon: <PersonIcon />,
      value: 3,
    },
  ];

  render() {
    return (
      <div className="navigator">
        <h1 style={{ color: "white" }}> Navigator </h1>
        {this.ButtonArray.map((button) => (
          <div>
            <button onClick={() => this.SwitchTables(button.value)} className="button">
              {button.icon}
              {button.text}
            </button>
          </div>
        ))}
      </div>
    );
  }
}
