import React from "react";
import Todos from "../Tables/Todos";
import Tags from "../Tables/Tags";
import Authors from "../Tables/Authors";

export default class DashBoard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      token: this.props.token,
    };
  }

  render() {
    return (
      <div className="dashboard">
        <div>
          {(() => {
            switch (this.props.table) {
              case 2:
                return <Tags token={this.state.token}></Tags>;
              case 3:
                return <Authors token={this.state.token}></Authors>;
              default:
                return <Todos token={this.state.token}></Todos>;
            }
          })()}
        </div>
      </div>
    );
  }
}
